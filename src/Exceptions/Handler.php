<?php

namespace Mediapress\VeronLogin\Exceptions;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Mediapress\VeronLogin\InformDevelopers\ExceptionPusher;

class Handler extends ExceptionHandler{



    public function render($request, Exception $exception)
    {
        ExceptionPusher::push($exception);

        return parent::render($request, $exception);
    }
}
