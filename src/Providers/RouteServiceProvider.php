<?php

namespace Mediapress\VeronLogin\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{

    protected $namespace = 'Mediapress\VeronLogin\Http\Controllers';


    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapPanelRoutes();


        //
    }

    /**
     * Define the "panel" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapPanelRoutes()
    {


        Route::middleware('web')
            ->namespace($this->namespace . '\Panel')
            ->prefix( 'mp-admin')
            ->group(__DIR__.'/../Routes/panel.php');
    }
}
