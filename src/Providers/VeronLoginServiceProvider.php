<?php

namespace Mediapress\VeronLogin\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Mediapress\VeronLogin\Exceptions\Handler;
use Illuminate\Contracts\Http\Kernel as HttpKernel;
use Mediapress\VeronLogin\Http\Kernel;


class VeronLoginServiceProvider extends ServiceProvider
{


    public const RESOURCES = 'Resources';
    public const VIEWS = 'views';

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Database' . DIRECTORY_SEPARATOR . 'migrations');
        $this->publishes([__DIR__.'/../Config' => config_path()], "VeronLoginPublishes");

        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR . self::RESOURCES . DIRECTORY_SEPARATOR . self::VIEWS . DIRECTORY_SEPARATOR . 'panel', 'VeronPanel');
    }

    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        $this->app->singleton(ExceptionHandler::class, Handler::class);
        $this->app->singleton(HttpKernel::class, Kernel::class);
    }
}
