<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTestsTable extends Migration {

	public function up()
	{
        if (Schema::hasTable('tests'))
            return;

		Schema::create('tests', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('website_id');
            $table->integer('sitemap_id')->nullable();
            $table->string('model_type');
            $table->string('admin');
            $table->string('name');
            $table->string('key')->nullable();
			$table->integer('value')->unsigned();
			$table->text('reason')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('tests');
	}
}
