<?php

namespace Mediapress\VeronLogin\InformDevelopers\RocketChat;


use GuzzleHttp\RequestOptions;

class Client
{

    protected $username;
    protected $password;

    /**
     * Client constructor.
     * @param null $username
     * @param null $password
     */
    public function __construct($username = null, $password = null)
    {
        if ($username == null) {
            $username = config("mediapress.rocket.exception_user","v3_exceptions");
        }

        if ($password == null) {
            $password = config("mediapress.rocket.exception_pass",'tXC94LpttK2xvja9N');
        }

        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @param $channel
     * @param $text
     */
    public function postMessage($channel, $text)
    {
        $this->post('chat.postMessage', [
            'channel' => '#' . $channel,
            'text' => $text,
            'collapsed' => true
        ]);
    }

    /**
     * @return \GuzzleHttp\Client
     */
    protected function getGuzzle()
    {
        return new \GuzzleHttp\Client([
            'base_uri' => config("mediapress.rocket.api",'https://rocket.mclck.com/api/v1/'),
            'http_errors' => false
        ]);
    }

    /**
     * @return array
     */
    protected function getHeaders()
    {
        $headers = ['Content-Type' => 'application/json'];

        $authentication = $this->getToken();
        if ($authentication) {
            $headers['X-User-Id'] = $authentication['userId'];
            $headers['X-Auth-Token'] = $authentication['authToken'];
        }

        return $headers;
    }

    /**
     * @return mixed
     */
    protected function getToken()
    {
        return \Cache::remember(md5('rocketchat_user_token'), 5, function () {
            $client = $this->getGuzzle();
            $r = $client->post('login', [
                RequestOptions::HEADERS => [
                    'Content-Type' => 'application/json'
                ],
                RequestOptions::JSON => [
                    'username' => $this->username,
                    'password' => $this->password
                ]
            ]);

            if ($r->getStatusCode() !== 200)
                return null;

            $response = json_decode($r->getBody()->getContents(), true);

            if ($response['status'] != 'success')
                return null;

            return [
                'userId' => $response['data']['userId'],
                'authToken' => $response['data']['authToken']
            ];
        });
    }

    /**
     * @param $endpoint
     * @return mixed|null
     */
    protected function get($endpoint)
    {
        $client = $this->getGuzzle();
        $request = $client->get($endpoint, [
            RequestOptions::HEADERS => $this->getHeaders()
        ]);

        if ($request->getStatusCode() !== 200)
            return null;

        return json_decode($request->getBody()->getContents(), true);
    }

    /**
     * @param $endpoint
     * @param $data
     * @return mixed|null
     */
    protected function post($endpoint, $data)
    {
        $client = $this->getGuzzle();
        $request = $client->post($endpoint, [
            RequestOptions::HEADERS => $this->getHeaders(),
            RequestOptions::JSON => $data
        ]);

        if ($request->getStatusCode() !== 200)
            return null;

        return json_decode($request->getBody()->getContents(), true);
    }

}
