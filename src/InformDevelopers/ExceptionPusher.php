<?php

namespace Mediapress\VeronLogin\InformDevelopers;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Session\TokenMismatchException;
use Mediapress\VeronLogin\InformDevelopers\RocketChat\Pusher;
use Prophecy\Exception\Doubler\ClassNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ExceptionPusher
{
    protected static $toIgnore = [
        IDPusherException::class,
        ValidationException::class,
        NotFoundHttpException::class,
        AuthenticationException::class,
        TokenMismatchException::class
    ];

    protected static $pushers = [
        'rocketchat' => Pusher::class
    ];

    protected static $push = true;

    /**
     * hide constructor
     */
    private function __construct()
    {
    }

    /**
     * @param $exception
     * @param array $pushers
     */
    public static function push($exception, $pushers = null)
    {

        if (array_search(get_class($exception), self::$toIgnore) !== false) {
            return;
        }

        if( function_exists( "customRocketPusher" ) ) {
            self::$push = customRocketPusher();
        }

        if( ! self::$push  ) {
            return;
        }

        if ($pushers == null) {
            $pushers = self::$pushers;
        }

        foreach ($pushers as $pusher) {
            if (!class_exists($pusher)) {
                throw new ClassNotFoundException($pusher . " does not exists!", $pusher);
            }
            call_user_func_array($pusher . "::push", [$exception]);
        }
    }
}
