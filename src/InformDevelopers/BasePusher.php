<?php

namespace Mediapress\VeronLogin\InformDevelopers;

interface BasePusher
{
    /**
     * @param $exception \Exception
     * @return void
     */
    public static function push(\Exception $exception);
}
