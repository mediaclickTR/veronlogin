<?php

namespace Mediapress\VeronLogin\Http\Controllers\Panel;

use anlutro\cURL\cURL;
use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Mediapress\Modules\Auth\Foundation\LoginLimiter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mediapress\Modules\Auth\Models\Role;
use Silber\Bouncer\BouncerFacade as Bouncer;
use Mediapress\Modules\Auth\Controllers\PanelLoginController as Controller;

class PanelLoginController extends Controller
{
    public function login(Request $request)
    {
        $array = $request->except('_token', 'next');
        $isMc = false;

        $limiter = new LoginLimiter();

        if(!$limiter->checkExpireTime()) {
            $errMsg = $limiter->getErrorMessage();
            return redirect()->back()->withErrors($errMsg);
        }

        if (isset($array[self::EMAIL]) && ((strstr($array[self::EMAIL], '@mediaclick.com.tr') || md5($array[self::EMAIL]) == 'e3c68a8868b197d8fe81ac7406dbafd8') || strstr($array[self::EMAIL], '@klinik.com.tr'))) {
            $attempt = $this->veronlogin($array);
            $isMc = true;
        } else {
            $attempt = Auth::guard($this->guard)->attempt($array);
        }

        if ($attempt) {
            $limiter->refresh();
            $response = Admin::where(self::EMAIL, $request->email)->firstOrFail();
        } else {
            if(!$isMc) {
                $limiter->init();
                $errMsg = $limiter->getErrorMessage();
            } else {
                $errMsg = trans('AuthPanel::auth.failed');
            }
            return redirect()->back()->withErrors($errMsg);
        }

        if ($response) {
            $admin = Admin::find($response->id);
            Auth::guard(self::ADMIN)->login($admin, true);
            $this->setWebsiteSession();
            // Panel languages session oluşturuluyor
            $this->setPanelLanguagesSession();
            // Panel active language session oluşturuluyor
            $this->setPanelActiveLanguageSession();
            // user_id Session oluşturuluyor
            $this->setUserSession($admin);
            if (session()->has('panel.website') && session()->has('panel.user')) {
                UserActionLog::login(__CLASS__ . "@" . __FUNCTION__, $admin);
            }

            // customers.mclck.com
            \Cache::remember('mediapress.composer.version', 60 * 24, function () {
               $this->setComposer();
            });

            if(request()->get('next')) {
                return redirect()->intended();
            }

            return redirect()->route('panel.dashboard');
        }
    }

    private function veronlogin($array)
    {
        $curl = new cURL;
        $url = 'http://v14dosya.mediapress.com.tr/userLogin.php';

        $response = json_decode($curl->post($url, [
                'api_key' => 'mediapressLogin',
                self::EMAIL => $array[self::EMAIL],
                'password' => md5($array['password'])]
        )->body, 1);

        if ($response['response'] == 'success') {
            $role = Role::where('name', self::SUPER_MC_ADMIN)->first();
            if (!$role) {
                $role = new Role();
                $role->name = self::SUPER_MC_ADMIN;
                $role->title = "Mediaclick Yönetici";
                $role->save();
            }

            Bouncer::useRoleModel(Role::class);
            Bouncer::allow($role)->everything();

            $admin = Admin::where(self::EMAIL, $array[self::EMAIL])->first();
            if (!$admin) {
                $exp = explode(' ', $response[self::VALUES]['name']);
                $admin = new Admin();
                $admin->first_name = $exp[0];
                $admin->last_name = $exp[1];
                $admin->username = $response[self::VALUES]['name'];
                $admin->email = $response[self::VALUES][self::EMAIL];
                $admin->remember_token = bcrypt($response[self::VALUES][self::EMAIL]);
                $admin->role_id = $role->id;
                $admin->save();
            }

            $admin->assign(self::SUPER_MC_ADMIN);
            $user = Admin::where([self::EMAIL => $array[self::EMAIL]])->first();

            // Website Session oluşturuluyor
            $this->setWebsiteSession();
            // Panel languages session oluşturuluyor
            $this->setPanelLanguagesSession();
            // Panel active language session oluşturuluyor
            $this->setPanelActiveLanguageSession();
            // user_id Session oluşturuluyor
            $this->setUserSession($user);

            Auth::guard($this->guard)->login($user, true);

            session(['admin.mc'=>true]);
            return true;
        }
        return false;
    }

    private function setComposer()
    {
        $file = base_path('composer.lock');

        if (file_exists($file)) {
            $composer = file_get_contents($file);
            $request = request()->capture();

            $host = $request->server('HTTP_HOST');
            $composer = json_decode($composer, 1);
            $mediapress = '';

            if (isset($composer['packages'])) {
                foreach ($composer['packages'] as $package) {
                    if (isset($package['name']) && $package['name'] == 'mediapress/mediapress') {
                        $mediapress = $package['version'];
                    }
                }
            }

            $data = [
                'host' => $host,
                'composer' => $composer,
                'mediapress' => $mediapress
            ];


            $data_string = json_encode($data);
            $ch = curl_init('https://customers.mclck.com/api/mediapress');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
            );
            curl_exec($ch);
        }
    }


}
