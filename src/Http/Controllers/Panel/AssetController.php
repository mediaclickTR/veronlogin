<?php

namespace Mediapress\VeronLogin\Http\Controllers\Panel;

use anlutro\cURL\cURL;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mediapress\Modules\Auth\Controllers\PanelLoginController as Controller;

class AssetController extends Controller
{

    public function css($file) {
        return response(file_get_contents(__DIR__.'/../../../Assets/css/'.$file))
            ->header('Content-Type', 'text/css');
    }

    public function js($file) {
        return response(file_get_contents(__DIR__.'/../../../Assets/js/'.$file))
            ->header('Content-Type', 'application/javascript');
    }

}
