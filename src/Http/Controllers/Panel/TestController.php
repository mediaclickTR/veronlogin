<?php

namespace Mediapress\VeronLogin\Http\Controllers\Panel;

use Illuminate\Http\Request;
use Mediapress\Http\Controllers\Controller;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\SitemapType;
use Mediapress\Modules\Content\Models\Website;
use Mediapress\VeronLogin\Models\Test;
use Validator;

use Illuminate\View\View;
use Illuminate\Http\JsonResponse;

/**
 * Class TestController
 * @package Mediapress\VeronLogin\Http\Controllers\Panel
 */
class TestController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        $sitemaps = $this->getSitemaps();

        $list = [];

        foreach ($sitemaps as $sitemap) {
            $array = [

                'id' => $sitemap->id,
                'name' => $sitemap->sitemapType->name,
                'type' => $sitemap->sitemapType->sitemap_type_type,
                'feature_tag' => $sitemap->feature_tag,
                'category' => $sitemap->category,
                'criteria' => $sitemap->criteria,
                'property' => $sitemap->property,
                'searchable' => $sitemap->searchable,
            ];

            $array += $this->getTestResults($sitemap);

            $list[] = $array;
        }

        $websiteTestResult = $this->getWebsiteTestResult();

        return view("VeronPanel::test.index", compact('list', 'websiteTestResult'));
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getTestModal(Request $request): array
    {
        $sitemapId = $request->get('sitemapId');
        $modelType = $request->get('modelType');

        if ($sitemapId == -1) {
            $dynamicTests = ['website', 'html'];
            $view = view("VeronPanel::test.layouts.website_modal", compact('modelType', 'dynamicTests'))->render();
        } else {
            $sitemap = $this->getSitemaps($sitemapId);
            $list = [
                'id' => $sitemap->id,
                'website_id' => $sitemap->website->id,
                'name' => $sitemap->sitemapType->name,
                'type' => $sitemap->sitemapType->sitemap_type_type,
                'feature_tag' => $sitemap->feature_tag,
                'category' => $sitemap->category,
                'criteria' => $sitemap->criteria,
                'property' => $sitemap->property,
                'searchable' => $sitemap->searchable,
            ];

            $dynamicTests = $this->getDynamicTests($modelType, $list);

            $view = view("VeronPanel::test.layouts.modal", compact('list', 'modelType', 'dynamicTests'))->render();
        }

        $contents = $this->getContents($sitemapId, $modelType);

        return [
            'dynamicTests' => $dynamicTests,
            'view' => $view,
            'contents' => $contents
        ];
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getRules(Request $request): string
    {
        $ruleType = $request->get('ruleType');
        $sitemapId = $request->get('sitemapId');
        $modelType = $request->get('modelType');

        $rules = $this->rules()[$ruleType] ?? [];

        $testModels = Test::where('sitemap_id', $sitemapId)
            ->where('model_type', $modelType)
            ->where('website_id', session('panel.website.id'))
            ->where('name', $ruleType)
            ->get(['key', 'value', 'reason']);

        $ruleValues = [];
        foreach ($testModels as $testModel) {
            $ruleValues[$testModel->key] = [
                'value' => $testModel->value,
                'reason' => $testModel->reason
            ];
        }

        return view("VeronPanel::test.layouts.rules", compact('rules', 'ruleType', 'ruleValues'))->render();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function saveForm(Request $request): JsonResponse
    {
        $data = $request->all();

        $validator = Validator::make($data, $this->getValidationRules());

        if ($validator->fails()) {
            $keys = $validator->errors()->keys();

            $keys = str_replace(['.', 'value'], ['_', 'yes'], $keys);
            return response()->json($keys, 422);
        }

        $websiteId = $data['website_id'];
        $sitemapId = $data['sitemap_id'] ?? null;
        $modelType = $data['model_type'];
        $admin = auth('admin')->user()->username;

        unset($data['_token'], $data['website_id'], $data['sitemap_id'], $data['model_type']);

        foreach ($data as $name => $dat) {
            foreach ($dat as $key => $d) {

                if (!isset($d['value'])) {
                    continue;
                }

                if($key == 'exist') {
                    if(($name == 'view' && $d['value'] == 1) || ($name != 'view' && $d['value'] == 2)) {
                        Test::where('sitemap_id', $sitemapId)
                            ->where('model_type', $modelType)
                            ->where('website_id', $websiteId)
                            ->where('name', $name)
                            ->delete();
                    }
                }

                Test::updateOrCreate(
                    [
                        'website_id' => $websiteId,
                        'sitemap_id' => $sitemapId,
                        'model_type' => $modelType,
                        'name' => $name,
                        'key' => $key,
                    ],
                    [
                        'admin' => $admin,
                        'value' => $d['value'] ?? null,
                        'reason' => $d['reason'] ?? null,
                    ]
                );
            }
        }


        return response()->json(['status' => true], 200);
    }

    /**
     * @return array|string[]
     */
    private function getValidationRules(): array
    {
        return [
            'view.exist.value' => 'filled',
            'view.redirect.value' => 'required_if:view.exist.value, 2',
            'view.redirect.reason' => 'required_if:view.redirect.value, 2',

            'otherSitemap.exist.value' => 'required_if:view.exist.value, 1',
            'otherSitemap.*.value' => "required_if:otherSitemap.exist.value, 1",
            'otherSitemap.*.reason' => 'required_if:otherSitemap.*.value, 2|nullable',
            'otherSitemap.exist.reason' => 'nullable',

            'image.exist.value' => 'required_if:view.exist.value, 1',
            'image.*.value' => "required_if:image.exist.value, 1",
            'image.*.reason' => 'required_if:image.*.value, 2|nullable',
            'image.exist.reason' => 'nullable',

            'form.exist.value' => 'required_if:view.exist.value, 1',
            'form.*.value' => "required_if:form.exist.value, 1",
            'form.*.reason' => 'required_if:form.*.value, 2|nullable',
            'form.exist.reason' => 'nullable',

            'slider.exist.value' => 'required_if:view.exist.value, 1',
            'slider.*.value' => "required_if:slider.exist.value, 1",
            'slider.*.reason' => 'required_if:slider.*.value, 2|nullable',
            'slider.exist.reason' => 'nullable',

            'general.*.value' => "required_if:view.exist.value, 1",
            'general.*.reason' => 'required_if:general.*.value, 2|nullable',

            'dynamicSitemap.*.value' => "required_if:viewExist, 1",
            'dynamicSitemap.*.reason' => 'required_if:dynamicSitemap.*.value, 2|nullable',

            'category.*.value' => "required_if:viewExist, 1",
            'category.*.reason' => 'required_if:category.*.value, 2|nullable',

            'criteria.*.value' => "required_if:viewExist, 1",
            'criteria.*.reason' => 'required_if:criteria.*.value, 2|nullable',

            'property.*.value' => "required_if:viewExist, 1",
            'property.*.reason' => 'required_if:property.*.value, 2|nullable',

            'pages.*.value' => "required_if:viewExist, 1",
            'pages.*.reason' => 'required_if:pages.*.value, 2|nullable',


            'html.langpart.value' => "required_if:type,website",
            'html.langpart.reason' => 'required_if:html.*.value, 2|nullable',

            'website.*.value' => "required_if:type,website",
            'website.*.reason' => 'required_if:website.*.value, 2|nullable',
        ];
    }

    /**
     * @param int|null $id
     * @return mixed
     */
    private function getSitemaps(int $id = null)
    {

        $sitemaps = Sitemap::whereHas('websites', function ($q) {
            $q->where('id', session('panel.website.id'));
        })
            ->status([Sitemap::ACTIVE, Sitemap::PASSIVE, Sitemap::DRAFT])
            ->with('sitemapType', 'details')
            ->orderBy('sitemap_type_id');

        if ($id == null) {
            $sitemaps = $sitemaps->get();
        } else {
            return $sitemaps->find($id);
        }

        return $sitemaps;
    }

    /**
     * @return array|\string[][]
     */
    private function rules(): array
    {
        return [
            'view' => [
                'redirect' => "Yönlendirme Yapıldı mı? (redirect)",
            ],
            'image' => [
                'resize' => 'Resimler boyutlandırıldı mı? (image->resize)',
                'resize_height' => 'Resimler için yükseklik gerekli ise eklendi mi? En boy sabit görsel (image->resize(["h"=>?]))',
                'renderable_size' => 'Resimler için renderableda en boy belirtildi mi?',
                'lazyload' => 'Resimler için lazyload eklendi mi?'
            ],
            'form' => [
                'validation' => 'Form validasyonları test edildi mi? PHP ve JS rules',
                'recaptcha' => 'Local, test ve live domainleri için recaptcha alındı mı?',
                'alert' => 'Formlar için hatalı dönüşte uyarı var mı? (alert)',
                'old_input' => 'Formda eski içerikler duruyor mu? (old input)',
                'thanks' => 'Formda teşekkür metni var mı?',
                'thanks_form_hide' => 'Formda teşekkür sayfasında form gizlendi mi?',
            ],
            'slider' => [
                'language' => 'Slider dile göre test edildi mi?',
                'redirect' => 'Link yönlendirmesi boş ve dolu şekilde test edildi mi?'
            ],
            'otherSitemap' => [
                'status' => 'Durum kontrolü yapıldı mı? (status)',
                'order' => 'Sıralama kontrolü Yapıldı mı? (order)',
            ],
            'general' => [
                'mediapress_pack' => 'MediapressPack yapıldı mı? (webpack)',
                'breadcrumb' => 'Breadcrumb eklendi mi?',
                'blade_queries' => 'Blade kısmındaki sorgular controllera taşındı mı?',
                'menu_active' => 'Menü de gösterimde aktif classı eklendi mi?'
            ],

            'dynamicSitemap' => [
                'datatable' => 'Datatable yeniden yapıldı mı?'
            ],
            'category' => [
                'status' => 'Durum kontrolü yapıldı mı? (status)',
                'order' => 'Sıralama kontrolü yapıldı mı? (order)',
            ],
            'criteria' => [
                'filter' => 'Filtrelerde boş olanlar gizlendi mi?',
            ],
            'property' => [
                'compare' => 'Karşılaştırma sayfası eklendi mi?'
            ],
            'pages' => [
                'status' => 'Durum kontrolü yapıldı mı? (status)',
                'order' => 'Sıralama kontrolü Yapıldı mı? (order)',
                'paginate' => 'Sayfalama yapıldı mı? (paginate)',
                'loading' => 'Sayfalama ajax ise loading ekranı eklendi mi?'
            ],

            'html' => [
                'langpart' => 'Sabit içerikler dile veya sayfa yapısına bağlandı mı? (langpart || sitemap->detail)'
            ],
            'website' => [
                'popup' => 'Popup test edildi mi?',
                '404' => '404 test edildi mi?',
                'language_redirect' => 'Dil yönledirmesi test edildi mi?',
                'search_page' => 'Arama sayfası test edildi mi?',
                'webp' => "Webp aktif edildi mi?"
            ]
        ];
    }

    /**
     * @param string $modelType
     * @param array $list
     * @return array|string[]
     */
    private function getDynamicTests(string $modelType, array $list): array
    {
        $dynamicTests = ["general"];
        if ($modelType == 'sitemap') {
            if ($list['type'] == 'dynamic') {
                $dynamicTests[] = 'dynamicSitemap';
                $dynamicTests[] = 'pages';
            }

            if ($list['category'] == 1) {
                $dynamicTests[] = 'category';
            }

            if ($list['criteria'] == 1) {
                $dynamicTests[] = 'criteria';
            }

            if ($list['property'] == 1) {
                $dynamicTests[] = 'property';
            }
        } elseif ($modelType == 'pages') {

        } elseif ($modelType == 'category') {

            $dynamicTests[] = 'pages';
        }

        return $dynamicTests;
    }

    /**
     * @param int $sitemapId
     * @param string $modelType
     * @return array
     */
    private function getContents(int $sitemapId, string $modelType): array
    {
        $id = $sitemapId == -1 ? null : $sitemapId;

        $testContents = Test::where('sitemap_id', $id)
            ->where('model_type', $modelType)
            ->where('website_id', session('panel.website.id'))
            ->where('key', 'exist')
            ->get()
            ->groupBy('name');

        $hold = [];
        foreach ($testContents as $name => $contents) {
            foreach ($contents as $content) {
                $hold[$name][$content->key] = [
                    'value' => $content->value,
                    'reason' => $content->reason,
                ];
            }
        }

        return $hold;
    }

    /**
     * @param Sitemap $sitemap
     * @return array
     */
    private function getTestResults(Sitemap $sitemap): array
    {
        $test = Test::where('sitemap_id', $sitemap->id)
            ->where('website_id', session('panel.website.id'));

        if($sitemap->category) {
            $temp = clone $test;
            $hold['category_test_result'] = $temp->where('model_type', 'category')->exists();
        }

        if($sitemap->sitemapType->sitemap_type_type == 'dynamic') {
            $temp = clone $test;
            $hold['pages_test_result'] = $temp->where('model_type', 'pages')->exists();
        }

        $hold['sitemap_test_result'] = $test->where('model_type', 'sitemap')->exists();

        return $hold;
    }

    /**
     * @return bool
     */
    private function getWebsiteTestResult(): bool
    {
        return Test::where('sitemap_id', null)
            ->where('website_id', session('panel.website.id'))
            ->where('model_type', 'website')
            ->exists();

    }
}

