<?php

namespace Mediapress\VeronLogin\Http\Controllers\Panel;

use Composer\Console\Application;
use Composer\Command\UpdateCommand;
use Mediapress\Http\Controllers\Controller;
use Mediapress\Modules\Content\Facades\Content;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\StreamOutput;

class UpdateController extends Controller
{

    public function index()
    {
        $versions = $this->getVersions();
        if (file_exists($this->getComposerLogs())) {
            file_put_contents($this->getComposerLogs(), '');
        }


        $crumbs = [
            [
                "key" => "tools_index",
                "text" => __('MPCorePanel::menu_titles.tools'),
                "icon" => "",
                "href" => route('Tools.index')
            ],
            [
                "key" => "tools.env",
                "text" => "Composer",
                "icon" => "",
                "href" => "javascript:void(0)"
            ],
        ];

        $breadcrumb = Content::getBreadcrumb($crumbs);

        return view("VeronPanel::composer.index", compact('versions', 'breadcrumb'));
    }

    public function composer()
    {

        require_once($this->getComposerDir().'/vendor/autoload.php');

        set_time_limit(-1);
        ini_set('memory_limit', '-1');
        $input = new StringInput('update'.' -vvv -d '.base_path(''));

        $output = new StreamOutput(fopen($this->getComposerLogs(), 'w'), 32, true);
        $app = new Application();
        $app->run($input, $output);
    }

    public function log()
    {

        if (file_exists($this->getComposerLogs())) {
            $log = explode("\n", file_get_contents($this->getComposerLogs()));

            $log = array_splice($log, -20, 20);

            $log = $this->colorize(implode('<br>', $log));

            return response($log);
        }
    }

    public function setComposerFiles(): void
    {
        putenv("COMPOSER_HOME=".storage_path('app/public'));
        $composer_file = $this->getComposerFile();

        if (!file_exists($composer_file)) {
            $this->downloadComposer();
        }
    }

    public function extractComposerFiles(): void
    {
        set_time_limit(-1);

        $composerDir = $this->getComposerDir();

        if (!file_exists($composerDir.'/vendor')) {
            mkdir($composerDir, 0755, 1);
            $composer = new \Phar($this->getComposerFile());
            $composer->extractTo($composerDir);
        }
    }

    private function getVersions(): array
    {

        $file = base_path('composer.lock');

        $composer = file_get_contents($file);

        $composer = json_decode($composer, 1);

        $holder = [];
        if (isset($composer['packages'])) {
            foreach ($composer['packages'] as $package) {
                if (isset($package['name']) && \Str::startsWith($package['name'], 'mediapress/')) {
                    $holder[] = [
                        'name' => $package['name'],
                        'currentVersion' => $package['version'],
                    ];
                }
            }
        }

        return $holder;
    }

    public function getPackagistVersion(string $packageName)
    {
        $lastVersion = "N/A";

        $file = "https://repo.packagist.org/p/".$packageName.".json";
        $composer = file_get_contents($file);

        $composer = json_decode($composer, 1);

        if (isset($composer['packages'][$packageName])) {
            $versions = $composer['packages'][$packageName];

            $versions = array_filter($versions, function ($item) {
                return !\Str::startsWith($item['version'], 'dev-') && !\Str::startsWith($item['version'], '5.');
            });

            uasort($versions, function ($a, $b) {

                if ($a['time'] == $b['time']) {
                    return 0;
                }
                return ($a['time'] > $b['time']) ? -1 : 1;
            });
            $lastVersion = array_shift($versions)['version'];
        }

        return $lastVersion;
    }

    private function downloadComposer(): void
    {
        $installerURL = 'https://getcomposer.org/installer';
        $installerFile = $this->getInstallerFile();

        if (!file_exists($installerFile)) {
            $ch = curl_init($installerURL);
            // curl_setopt($ch, CURLOPT_CAINFO, __DIR__ . '/cacert.pem');
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_FILE, fopen($installerFile, 'w+'));
            if (!curl_exec($ch)) {
                echo 'Error downloading '.$installerURL.PHP_EOL;
                die();
            }
        }

        $argv = array();
        chdir(storage_path('app/public/'));
        //header('Location: ' . request()->url());
        include $installerFile;
        flush();
    }

    private function getComposerFile(): string
    {
        return storage_path('app/public/composer.phar');
    }

    private function getComposerDir(): string
    {
        return storage_path('app/public/composer');
    }

    private function getInstallerFile(): string
    {
        return storage_path('app/public/installer.php');
    }

    private function getComposerLogs(): string
    {
        return storage_path('app/public/composer.log');

    }

    private function colorize($string): string
    {
              $rex = [
            "\e[30m",
            "\e[31m",
            "\e[32m",
            "\e[33m",
            "\e[34m",
            "\e[35m",
            "\e[36m",
            "\e[37m",
            "\e[39m",
            "<warning>",
            "</warning>",
        ];


        $colors = [
           '<span style="color:black ">',
           '<span style="color:red ">',
           '<span style="color:#38900a ">',
           '<span style="color:#e5dd34 ">',
           '<span style="color:blue ">',
           '<span style="color:magenta ">',
           '<span style="color:cyan ">',
           '<span style="color:white ">',
            '</span>',
            '<span style="background-color:#a39d16 ">',
            '</span>'


        ];


        return str_replace($rex,$colors,$string);


    }
}
