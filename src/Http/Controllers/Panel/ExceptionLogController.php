<?php

namespace Mediapress\VeronLogin\Http\Controllers\Panel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Mediapress\Modules\Auth\Controllers\PanelLoginController as Controller;

class ExceptionLogController extends Controller{

    public function __construct()
    {
        $this->middleware('guest', ['except' => ['index','clearExceptions','json','getFile']]);
    }
    public function index(Request $request)
    {
        $to_open = $request->get('f', null);
        if ($to_open) {
            $to_open = $this->renderExceptionLog($to_open);
        }



        return view("VeronPanel::exception_logs.index", compact( 'to_open'));
    }

    public function json(Request $request)
    {
        $fTree = [];
        $_files = Storage::disk('local')->allFiles('exceptions');

        $_files = collect($_files)->filter(function ($q) {
            return strstr($q, '.tmp') == null;
        })->values()->all();

        foreach ($_files as $file) {
            $tokens = explode("/", $file);

            $y = $tokens[1];
            $m = $tokens[2];
            $d = $tokens[3];
            $f = $tokens[4];

            if (!isset($fTree[$y]))
                $fTree[$y] = [];
            if (!isset($fTree[$y][$m]))
                $fTree[$y][$m] = [];
            if (!isset($fTree[$y][$m]))
                $fTree[$y][$m][$d] = [];

            $fTree[$y][$m][$d][] = [$f, $file];
        }

        return response()->json($fTree);
    }

    public function getFile(Request $request, $filePath)
    {
        if ($filePath === 'clear')
            return $this->clearExceptions();

        $htmlContent = $this->renderExceptionLog($filePath);

        return response()->json([
            'path' => $filePath,
            'content' => $htmlContent
        ]);
    }

    protected function renderExceptionLog($filePath)
    {
        $parseDown = new \Parsedown();

        $filePath =
            collect(Storage::disk('local')->allFiles('exceptions'))
                ->filter(function ($e) {
                    if (strstr($e, '.tmp') !== false) {
                        return false;
                    }

                    return true;
                })
                ->filter(function ($f) use ($filePath) {
                    return strstr($f, $filePath) !== false;
                })
                ->first();

        $fileContent = '`Not Found.`';
        if (Storage::disk('local')->exists($filePath))
            $fileContent = Storage::disk('local')->get($filePath);

        return
            $parseDown
                ->setBreaksEnabled(true)
                ->setUrlsLinked(true)
                ->text($fileContent);
    }

    protected function clearExceptions()
    {
        Storage::disk('local')->delete(
            Storage::disk('local')->allFiles('exceptions')
        );
        return Redirect::back();
    }
}
