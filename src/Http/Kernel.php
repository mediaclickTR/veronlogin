<?php

namespace Mediapress\VeronLogin\Http;


use Mediapress\Http\Middleware\AfterMiddleware;
use Mediapress\Http\Middleware\AuthenticateMiddleware;
use Mediapress\Http\Middleware\DefaultLng;
use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Mediapress\Http\Middleware\Flow;
use Mediapress\Modules\Auth\Middleware\RedirectIfAuthenticated;

class Kernel extends HttpKernel
{
    protected $middlewareGroups = [
        'adminer' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,

            // you may create customized middleware to fit your needs
            // example uses Laravel default authentication (default protection)
            //  \Illuminate\Auth\Middleware\Authenticate::class,
        ],
    ];
}
