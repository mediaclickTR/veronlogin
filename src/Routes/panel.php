<?php



Route::get('_veronlogin/css/{file}','AssetController@css');
Route::get('_veronlogin/js/{file}','AssetController@js');

Route::group(['middleware' => ['panel.auth']], function(){
    Route::get('ExceptionLogs','ExceptionLogController@index');
    Route::get('ExceptionLogs/clear','ExceptionLogController@clearExceptions')->name('admin.exception_log.clear');
    Route::get('ExceptionLogs/json','ExceptionLogController@json');
    Route::get('ExceptionLogs/{filepath}','ExceptionLogController@getFile');


    Route::any('/adminer', 'DatabaseController@index');
    Route::get('/adminer/default.css', 'DatabaseController@css')->name('adminer.css');
    Route::get('/adminer/functions.js', 'DatabaseController@js')->name('adminer.js');


    Route::get('composer','UpdateController@index')->name('composer.index');
    Route::get('composer-files','UpdateController@setComposerFiles')->name('composer.files');
    Route::get('composer-extract','UpdateController@extractComposerFiles')->name('composer.extract');
    Route::get('composer-update','UpdateController@composer')->name('composer.update');
    Route::get('composer-log','UpdateController@log')->name('composer.logs');
    Route::get('composer-packagist/{packageName}','UpdateController@getPackagistVersion')->name('composer.getPackagistVersion')
        ->where('packageName', '(.*)');


    Route::group(['prefix' => 'Test', 'as' => 'Test.'], function () {

        Route::get('/','TestController@index')->name('index');
        Route::get('/modal','TestController@getTestModal')->name('getTestModal');
        Route::get('/rules','TestController@getRules')->name('getRules');
        Route::post('/save-form','TestController@saveForm')->name('saveForm');
    });




    Route::get("/phpinfo",'GeneralController@phpinfo');
});
