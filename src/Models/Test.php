<?php

namespace Mediapress\VeronLogin\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\Foundation\KeyValueCollection;
use Mediapress\Modules\Content\Controllers\InstagramController;
use Illuminate\Database\Eloquent\SoftDeletes;

class Test extends Model
{
    protected $table = "tests";
    protected $guarded = ["id"];
}
