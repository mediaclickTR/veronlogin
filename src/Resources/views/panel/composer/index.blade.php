@extends('MPCorePanel::inc.app')

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="row">
        <div class="col-md-12">
            <div class="bd-example white mb-5">
                <div class="p-4">
                    <div class="row mb-4">
                        <div class="col-8 offset-2">
                            <table class="table">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Package Name</th>
                                    <th scope="col" style="text-align: center">Current Version</th>
                                    <th scope="col" style="text-align: center">Last Packagist Version</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($versions as $version)
                                    <tr>
                                        <td><strong>{{ $version['name'] }}</strong></td>
                                        <td style="text-align: center;" class="currentVersion">{{ $version['currentVersion'] }}</td>
                                        <td style="text-align: center" class="{{ str_replace('/', '-', $version['name']) }}"></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="text-center">
                                <a href="javascript:void(0);"
                                   onclick="installComposerFiles()"
                                   class="btn btn-primary install {{ file_exists(storage_path('app/public/composer/vendor')) ? 'd-none' : '' }}">
                                    Install Composer Files
                                </a>
                                <a href="javascript:void(0);"
                                   onclick="composerUpdate()"
                                   class="btn btn-primary update {{ file_exists(storage_path('app/public/composer/vendor')) ? '' : 'd-none' }}">
                                    Composer Update
                                </a>
                            </div>
                            <div class="mt-3" id="messages">
                                <pre class="{{ file_exists(storage_path('app/public/composer/vendor')) ? '' : 'd-none' }}" style="margin-left: 25%">
   ______
  / ____/ ___    ____ ___    ____    ____    _____  ___    _____
 / /    / __ \  / __ `__ \  / __ \  / __ \  / ___/ / _ \  / ___/
/ /___ / /_/ / / / / / / / / /_/ / / /_/ / (__  ) /  __/ / /
\____/ \____/ /_/ /_/ /_/ / .___/  \____/ /____/  \___/ /_/
                         /_/
                                </pre>
                                <p class="message d-none">
                                </p>
                            </div>
                            <div class="text-center mpUpdate d-none">
                                <a href="javascript:void(0);" onclick="mpUpdate()" class="btn btn-lg btn-block btn-outline-primary mp-update">MP Update Komutu</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<style>
    p.message {
        display: block;
        font-size: 12px;
        padding: 20px 10px;
        background-color: #2b2b2b;
        color: white;

    }

    table tr td {
        line-height: 10px !important;
    }
</style>
@push('scripts')
    <script>

        @foreach($versions as $version)
            $.ajax({
                'url': "{{ route('composer.getPackagistVersion', ['packageName' => $version['name']]) }}",
                'method': "get",
                success: function (response) {
                    var el = $('td.{{str_replace('/', '-', $version['name'])}}');
                    el.html(response);
                    var currentVersion = el.siblings('td.currentVersion').html();

                    if(currentVersion < response) {
                        el.siblings('td.currentVersion').css('color','red');
                    }
                }
            })
        @endforeach


        function installComposerFiles() {
            $('p.message').removeClass('d-none');

            $.ajax({
                'url': "{{ route('composer.files') }}",
                'method': "get",
                success: function (response) {
                    $('p.message').removeClass('d-none');
                    $('p.message').html(response);

                    $('a.install').addClass('d-none');
                    $('a.update').removeClass('d-none');


                    $.ajax({
                        'url': "{{ route('composer.extract') }}",
                        'method': "get",
                    })

                    $('#messages pre').removeClass('d-none');
                }
            })
        }


        function composerUpdate() {
            $('#messages').after('<iframe src="{!! route("composer.update") !!}" frameborder="0" width="1" height="1"></iframe>');
            logAjax()
        }

        function logAjax() {
            $('#messages pre').removeClass('d-none');
            $('p.message').removeClass('d-none');
            $.ajax({
                'url': "{{ route('composer.logs') }}",
                'method': "get",
                success: function (response) {
                    if(response) {
                        $('p.message').html(response);
                    }

                    if(response && response.search("Package manifest generated successfully.") != -1) {
                        $('p.message').html("<strong class='h3 text-success font-weight-bold' style='margin-left: 20%'>Composer Update Successfully Executed !!</strong>");
                        $('.mpUpdate').removeClass('d-none');
                    } else {
                        setTimeout(function () {
                            logAjax()
                        }, 1000);
                    }
                },
                error: function () {
                    setTimeout(function () {
                        logAjax()
                    }, 1000);
                }
            })
        }


        function mpUpdate() {
            $('.mp-update').removeClass('btn-outline-primary');
            $('.mp-update').addClass('btn-success');
            $('.mp-update').html("MP Update Komutu Başladı !! <img width='30' class='float-right' src='{{ asset("vendor/mediapress/images/loading.gif") }}'>");

            $.get("{!! route('Tools.mpupdate.index') !!}", function (data) {

                setTimeout(function() {
                    $('.mp-update').removeClass('btn-success');
                    $('.mp-update').addClass('btn-warning');
                    $('.mp-update').text("MP Update Komutu Tamamlandı !!");

                    setTimeout(function() {
                        $('.mp-update').removeClass('btn-warning');
                        $('.mp-update').addClass('btn-outline-primary');
                        $('.mp-update').text("MP Update Komutu");
                    }, 2000)
                }, 5000)
            });
        }
    </script>
@endpush
