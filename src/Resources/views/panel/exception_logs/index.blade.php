@extends('MPCorePanel::inc.app')

@section('content')
    <h1>Exception Logs</h1>
    <div class="row">
        <div class="col-md-4">
            <div class="btn-group">
                <a href="{{ route('admin.exception_log.clear') }}" class="btn btn-primary">Clear Exceptions</a>
            </div>
        </div>
        <div class="col-md-8">
            <form method="get" class="form-inline">
                <div class="form-group">
                    <label for="f" style="display: inline-block">Exception ID:</label>
                    <input id="f" name="f" type="text" class="form-control"/>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn">Show</button>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div id="file-tree"></div>
        </div>
        <div class="col-md-8" id="md-target">
            @if($to_open)
                {!! $to_open !!}
            @else
                Please select an exception to view.
            @endif
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/mediapress/js/plugins/jstree/themes/default/style.min.css') }}"/>
@endpush

@push('scripts')
    <script src="{{ asset('vendor/mediapress/js/plugins/jstree/jstree.min.js') }}"></script>
    <script>
        $(() => {
            const mdTarget = $('#md-target');

            const fTree = $('#file-tree').jstree({
                core: {
                    data: function (obj, callback) {
                        $.get('{{ panel('ExceptionLogs/json') }}')
                            .then(data => {
                                let tree = [];

                                Object.keys(data).forEach((y) => {
                                    let yChildren = [];
                                    let yNode = {
                                        id: y,
                                        text: y,
                                        state: {
                                            opened: true
                                        }
                                    };

                                    Object.keys(data[y]).forEach((m) => {
                                        let mChildren = [];
                                        let mNode = {
                                            id: `${y}_${m}`,
                                            text: m,
                                            state: {
                                                opened: true
                                            }
                                        };

                                        Object.keys(data[y][m]).forEach((d) => {
                                            let dChildren = [];
                                            let dNode = {
                                                id: `${y}_${m}_${d}`,
                                                text: d,
                                                state: {
                                                    opened: true
                                                }
                                            };

                                            data[y][m][d].forEach((log) => {
                                                let logNode = {
                                                    id: log[0],
                                                    text: log[0],
                                                    fullPath: log[1]
                                                };

                                                dChildren.push(logNode);
                                            });

                                            dNode.children = dChildren;
                                            mChildren.push(dNode);
                                        });

                                        mNode.children = mChildren;
                                        yChildren.push(mNode);
                                    });

                                    yNode.children = yChildren;
                                    tree.push(yNode);
                                });

                                callback.call(this, tree);
                            });
                    }
                }
            });

            fTree.on('select_node.jstree', (e, selected) => {
                let path = selected.node.original.fullPath;
                path = path.split('.');
                path = path[path.length - 2].split('/');
                path = path[path.length - 1];

                $('#f').val(path);

                $.get(`{{ panel('ExceptionLogs') }}/${path}`)
                    .then((file) => {
                        mdTarget.html(file.content);
                    });
            });
        });
    </script>
@endpush
