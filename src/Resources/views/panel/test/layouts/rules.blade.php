@foreach($rules as $key => $rule)
    @php
        $value = $ruleValues[$key]['value'] ?? null;
        $reason = $ruleValues[$key]['reason'] ?? '';

        $inputName = $ruleType . "[" . $key . "][value]";
        $inputId = $ruleType . "_" . $key;
    @endphp
    <div class="form-group row">
        <div class="col-md-8">
            <label class="col-form-label">{{ $rule }}</label>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-6">
                    <div class="checkbox">
                        <label for="{{$inputId}}_yes">Evet</label>
                        <input type="radio" class="checkbox child" data-reason="{{$inputId . '_reason'}}"
                               {{ $value == 1 ? 'checked' : '' }}
                               name="{{$inputName}}" id="{{$inputId}}_yes" value="1">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="checkbox">
                        <label for="{{$inputId}}_no">Hayır</label>
                        <input type="radio" class="checkbox child" data-reason="{{$inputId . '_reason'}}"
                               {{ $value == 2 ? 'checked' : '' }}
                               name="{{$inputName}}" id="{{$inputId}}_no" value="2">
                    </div>
                </div>
            </div>
            <div class="mt-3 {{ $value == 2 ? '' : 'd-none' }}">
                <textarea name="{{$ruleType}}[{{$key}}][reason]" id="{{$inputId . '_reason'}}"
                          placeholder="Nedenini açıklayınız...">{{ $reason }}</textarea>

                <div class="col-md-12 text-danger d-none validationError" style="font-size: 12px">
                    *Bu Alan Zorunludur.
                </div>
            </div>
        </div>

        <div class="col-md-12 text-danger d-none validationError" style="font-size: 12px">
            *Bu Alan Zorunludur.
        </div>
    </div>
@endforeach
