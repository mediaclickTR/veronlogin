<form action="" id="testForm">
    @csrf

    <input type="hidden" name="website_id" value="{{ session('panel.website.id') }}">
    <input type="hidden" name="model_type" value="{{ $modelType }}">

    <!-- HTML Test Start -->
    <div class="htmlTest testBox">
        <div class="form-group row">
            <div class="col-md-12">
                <label class="col-form-label"><strong>HTML</strong></label>
            </div>
        </div>

        <div class="htmlRules ml-4"></div>
    </div>
    <!-- HTML Test Finish -->

    <!-- Website Test Start -->
    <div class="websiteTest testBox">
        <div class="form-group row">
            <div class="col-md-12">
                <label class="col-form-label"><strong>Website</strong></label>
            </div>
        </div>

        <div class="websiteRules ml-4"></div>
    </div>
    <!-- Website Test Finish -->
</form>
