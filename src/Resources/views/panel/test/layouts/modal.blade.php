<form action="" id="testForm">
    @csrf

    <input type="hidden" name="website_id" value="{{ $list['website_id'] }}">
    <input type="hidden" name="sitemap_id" value="{{ $list['id'] }}">
    <input type="hidden" name="model_type" value="{{ $modelType }}">
    <br>

    <!-- View Test Start -->
    <div class="viewTest">
        <div class="form-group row">
            <div class="col-md-8">
                <label class="col-form-label" id="view_exist_yes">Bu sayfanın çıktısı var mı? (view)</label>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6">
                        <div class="checkbox">
                            <label for="viewYes">Evet</label>
                            <input type="radio" class="checkbox main" data-type="view"
                                   name="view[exist][value]" value="1"
                                   id="viewYes">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="checkbox">
                            <label for="viewNo">Hayır</label>
                            <input type="radio" class="checkbox main" data-type="view"
                                   name="view[exist][value]" value="2"
                                   id="viewNo">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 text-danger d-none validationError" style="font-size: 12px">
                *Bu Alan Zorunludur.
            </div>
        </div>

        <div class="viewRules ml-4"></div>

    </div>
    <!-- View Test Finish -->

    <!-- Other Sitemap Test Start -->
    <div class="otherSitemapTest testBox d-none">
        <div class="form-group row">
            <div class="col-md-8">
                <label class="col-form-label" id="otherSitemap_exist_yes">Bu sayfada başka sayfa yapısı içeriği var mı?</label>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6">
                        <div class="checkbox">
                            <label for="otherSitemapYes">Evet</label>
                            <input type="radio" class="checkbox main" data-type="otherSitemap"
                                   name="otherSitemap[exist][value]" value="1"
                                   id="otherSitemapYes">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="checkbox">
                            <label for="otherSitemapNo">Hayır</label>
                            <input type="radio" class="checkbox main" data-type="otherSitemap"
                                   name="otherSitemap[exist][value]" value="2"
                                   id="otherSitemapNo">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 text-danger d-none validationError" style="font-size: 12px">
                *Bu Alan Zorunludur.
            </div>
        </div>

        <div class="otherSitemapRules ml-4"></div>

    </div>
    <!-- Other Sitemap Test Finish -->

    <!-- Image Test Start -->
    <div class="imageTest testBox d-none">
        <div class="form-group row">
            <div class="col-md-8">
                <label class="col-form-label" id="image_exist_yes">Bu sayfada görsel var mı?</label>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6">
                        <div class="checkbox">
                            <label for="imageYes">Evet</label>
                            <input type="radio" class="checkbox main" data-type="image"
                                   name="image[exist][value]" value="1"
                                   id="imageYes">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="checkbox">
                            <label for="imageNo">Hayır</label>
                            <input type="radio" class="checkbox main" data-type="image"
                                   name="image[exist][value]" value="2"
                                   id="imageNo">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 text-danger d-none validationError" style="font-size: 12px">
                *Bu Alan Zorunludur.
            </div>
        </div>

        <div class="imageRules ml-4"></div>

    </div>
    <!-- Image Test Finish -->

    <!-- Form Test Start -->
    <div class="formTest testBox d-none">
        <div class="form-group row">
            <div class="col-md-8">
                <label class="col-form-label" id="form_exist_yes">Bu sayfada form var mı?</label>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6">
                        <div class="checkbox">
                            <label for="formYes">Evet</label>
                            <input type="radio" class="checkbox main" data-type="form"
                                   name="form[exist][value]" value="1"
                                   id="formYes">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="checkbox">
                            <label for="formNo">Hayır</label>
                            <input type="radio" class="checkbox main" data-type="form"
                                   name="form[exist][value]" value="2"
                                   id="formNo">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 text-danger d-none validationError" style="font-size: 12px">
                *Bu Alan Zorunludur.
            </div>
        </div>

        <div class="formRules ml-4"></div>

    </div>
    <!-- Form Test Finish -->

    <!-- Slider Test Start -->
    <div class="sliderTest testBox d-none">
        <div class="form-group row">
            <div class="col-md-8">
                <label class="col-form-label" id="slider_exist_yes">Bu sayfada slider var mı?</label>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6">
                        <div class="checkbox">
                            <label for="sliderYes">Evet</label>
                            <input type="radio" class="checkbox main" data-type="slider"
                                   name="slider[exist][value]" value="1"
                                   id="sliderYes">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="checkbox">
                            <label for="sliderNo">Hayır</label>
                            <input type="radio" class="checkbox main" data-type="slider"
                                   name="slider[exist][value]" value="2"
                                   id="sliderNo">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 text-danger d-none validationError" style="font-size: 12px">
                *Bu Alan Zorunludur.
            </div>
        </div>

        <div class="sliderRules ml-4"></div>

    </div>
    <!-- Slider Test Finish -->

    <!-- General Test Start -->
    <div class="generalTest testBox d-none">
        <div class="form-group row">
            <div class="col-md-12">
                <label class="col-form-label"><strong>Genel</strong></label>
            </div>
        </div>

        <div class="generalRules ml-4"></div>
    </div>
    <!-- General Test Finish -->
@if($modelType == 'sitemap')
    @if($list['type'] == 'dynamic')

        <!-- Dynamic Sitemap Test Start -->
        <div class="dynamicSitemapTest testBox d-none">
            <div class="form-group row">
                <div class="col-md-12">
                    <label class="col-form-label"><strong>Dynamic Sitemap</strong></label>
                </div>
            </div>

            <div class="dynamicSitemapRules ml-4"></div>
        </div>
        <!-- Dynamic Sitemap Test Finish -->


        @if($list['category'] == 1)
            <!-- Category Test Start -->
            <div class="categoryTest testBox d-none">
                <div class="form-group row">
                    <div class="col-md-12">
                        <label class="col-form-label"><strong>Sitemap Category</strong></label>
                    </div>
                </div>

                <div class="categoryRules ml-4"></div>

            </div>
            <!-- Category Test Finish -->
        @endif


        @if($list['criteria'] == 1)
            <!-- Criteria Test Start -->
            <div class="criteriaTest testBox d-none">
                <div class="form-group row">
                    <div class="col-md-12">
                        <label class="col-form-label"><strong>Sitemap Criteria</strong></label>
                    </div>
                </div>

                <div class="criteriaRules ml-4"></div>

            </div>
            <!-- Criteria Test Finish -->
        @endif


        @if($list['property'] == 1)
            <!-- Property Test Start -->
            <div class="propertyTest testBox d-none">
                <div class="form-group row">
                    <div class="col-md-12">
                        <label class="col-form-label"><strong>Sitemap Property</strong></label>
                    </div>
                </div>

                <div class="propertyRules ml-4"></div>

            </div>
            <!-- Property Test Finish -->
        @endif


        <!-- Pages Test Start -->
        <div class="pagesTest testBox d-none">
            <div class="form-group row">
                <div class="col-md-12">
                    <label class="col-form-label"><strong>Pages</strong></label>
                </div>
            </div>

            <div class="pagesRules ml-4"></div>
        </div>
        <!-- Pages Test Finish -->
    @endif
@elseif($modelType == 'category')
    <!-- Pages Test Start -->
    <div class="pagesTest testBox d-none">
        <div class="form-group row">
            <div class="col-md-12">
                <label class="col-form-label"><strong>Pages</strong></label>
            </div>
        </div>

        <div class="pagesRules ml-4"></div>
    </div>
    <!-- Pages Test Finish -->
@endif
</form>

