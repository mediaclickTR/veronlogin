@extends('MPCorePanel::inc.app')

@section('content')
    <div class="page-content p-0">
        <div class="p-30 mt-4">
            <table id="sitemaps-table" class="m-0 table table-striped">
                <tr>
                    <td>Website</td>
                    <td></td>
                    <td></td>
                    <td>
                        <a href="javascript:void(0)"
                           onclick="showTestModal(-1, 'website', 'Website')"
                           class="btn btn-primary">Test Et</a>
                    </td>
                    <td style="width: 20%">
                        @if($websiteTestResult)
                            <button type="button" class="btn btn-sm btn-light">
                                <span class="badge badge-success"><i class="fa fa-check" aria-hidden="true"></i></span>
                            </button>
                        @else
                            <button type="button" class="btn btn-sm btn-light">
                                <span class="badge badge-danger"><i class="fa fa-times" aria-hidden="true"></i></span>
                            </button>
                        @endif
                    </td>
                </tr>
                @foreach($list as $sitemap)
                    <tr>
                        <td>{!! $sitemap['name'] !!} / Sitemap</td>
                        <td>{!! $sitemap['type'] !!}</td>
                        <td><span class="badge badge-primary">{!! $sitemap['feature_tag'] !!}</span></td>
                        <td>
                            <a href="javascript:void(0)"
                               onclick="showTestModal({!! $sitemap['id'] !!}, 'sitemap', '{{ $sitemap['name'] . ' / Sitemap' }}')"
                               class="btn btn-primary">Test Et</a>
                        </td>
                        <td style="width: 20%">
                            @if($sitemap['sitemap_test_result'])
                                <button type="button" class="btn btn-sm btn-light">
                                    <span class="badge badge-success"><i class="fa fa-check" aria-hidden="true"></i></span>
                                </button>
                            @else
                                <button type="button" class="btn btn-sm btn-light">
                                    <span class="badge badge-danger"><i class="fa fa-times" aria-hidden="true"></i></span>
                                </button>
                            @endif
                        </td>
                    </tr>
                    @if($sitemap['category'])
                        <tr>
                            <td>{!! $sitemap['name'] !!} / Category</td>
                            <td>{!! $sitemap['type'] !!}</td>
                            <td></td>
                            <td>
                                <a href="javascript:void(0)"
                                   onclick="showTestModal({!! $sitemap['id'] !!},'category', '{{ $sitemap['name'] . ' / Category' }}')"
                                   class="btn btn-primary">Test Et</a>
                            </td>
                            <td style="width: 20%">
                                @if($sitemap['category_test_result'])
                                    <button type="button" class="btn btn-sm btn-light">
                                        <span class="badge badge-success"><i class="fa fa-check" aria-hidden="true"></i></span>
                                    </button>
                                @else
                                    <button type="button" class="btn btn-sm btn-light">
                                        <span class="badge badge-danger"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </button>
                                @endif
                            </td>
                        </tr>
                    @endif
                    @if($sitemap['type'] == 'dynamic')
                        <tr>
                            <td>{!! $sitemap['name'] !!} / Page</td>
                            <td>{!! $sitemap['type'] !!}</td>
                            <td></td>
                            <td>
                                <a href="javascript:void(0)"
                                   onclick="showTestModal({!! $sitemap['id'] !!},'pages', '{{ $sitemap['name'] . ' / Page' }}')"
                                   class="btn btn-primary">Test Et</a>
                            </td>
                            <td style="width: 20%">
                                @if($sitemap['pages_test_result'])
                                    <button type="button" class="btn btn-sm btn-light">
                                        <span class="badge badge-success"><i class="fa fa-check" aria-hidden="true"></i></span>
                                    </button>
                                @else
                                    <button type="button" class="btn btn-sm btn-light">
                                        <span class="badge badge-danger"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </button>
                                @endif
                            </td>
                        </tr>
                    @endif
                @endforeach
            </table>
        </div>
    </div>

    <div class="modal fade" id="testModal" tabindex="-1" role="dialog" aria-labelledby="testModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="testModalLabel">Test</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" id="saveTestForm">Kaydet</button>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script>
        var sitemapId;
        var modelType;

        var viewStatus = false;


        $(document).delegate('input.main', 'ifChecked', function () {

            var type = $(this).attr('data-type')

            setRules($(this), type);
        })

        $(document).delegate('input.main', 'ifUnchecked', function () {
            var type = $(this).attr('data-type')
            $('.' + type + 'Rules').html("");
        })

        $(document).delegate('input.child', 'ifChecked', function () {
            var reasonText = $('#' + $(this).attr('data-reason'));

            if ($(this).val() == 2) {
                reasonText.parent('div').removeClass('d-none');
            }
        })

        $(document).delegate('input.child', 'ifUnchecked', function () {
            var reasonText = $('#' + $(this).attr('data-reason'));

            reasonText.parent('div').addClass('d-none');
            reasonText.val("")
        })

        function showTestModal(id, type, name) {
            sitemapId = id;
            modelType = type;

            var requestData = {
                sitemapId: sitemapId,
                modelType: modelType
            }
            $.ajax({
                'url': "{{ route('Test.getTestModal') }}",
                'data': requestData,
                'method': "GET",
                success: function (response) {
                    $('#testModal .modal-body').html(response.view);

                    var dynamicTests = response.dynamicTests
                    $.each(dynamicTests, function (k, v) {
                        getRules(v);
                    })

                    $('#testModalLabel').html(name);

                    $.each(response.contents, function(name, content) {
                        $.each(content, function(key, cont) {
                            $("input[name='"+name+"["+key+"][value]'][value='"+cont.value+"']").iCheck('check');
                        })
                    })

                    $('#testModal').modal('show');
                }
            })
        }

        function setRules(element, type) {

            if(element.val() == 1) {
                if(type == 'view') {
                    $('.' + type + 'Rules').html("");
                    $('.testBox').removeClass('d-none');
                } else {
                    getRules(type)
                }
            } else {
                if(type == 'view') {
                    getRules(type)
                    $('.testBox').addClass('d-none');
                    $('.testBox input.checkbox').iCheck('uncheck');
                } else {
                    $('.' + type + 'Rules').html("");
                }
            }
        }

        function getRules(ruleType) {

            var requestData = {
                ruleType: ruleType,
                sitemapId: sitemapId == -1 ? null : sitemapId,
                modelType: modelType
            }
            $.ajax({
                'url': "{{ route('Test.getRules') }}",
                'method': "GET",
                'data': requestData,
                success: function (response) {
                    $('.' + ruleType + 'Rules').html(response);
                }
            })
            .done(function () {
                updateCheckbox();
            })
        }

        function updateCheckbox() {
            $('input.checkbox').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue'
            });
        }

        $('#saveTestForm').on('click', function () {
            var formData = $('#testForm').serialize();

            $.ajax({
                'url': "{{ route('Test.saveForm') }}",
                'data': formData,
                'method': "POST",
                success: function (response) {
                    $('#testModal').modal('hide');
                    location.reload();
                },
                error: function (response) {

                    $('.validationError').addClass('d-none');

                    $.each(response.responseJSON, function (k, v) {
                        $('label[for="' + v + '"], label#' + v).closest('.form-group').find('.validationError').removeClass('d-none');
                        $('#' + v).siblings('.validationError').removeClass('d-none');
                    })
                }
            })
        })
    </script>
@endpush
