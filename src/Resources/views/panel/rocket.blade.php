@if(auth()->guard('admin')->check())
    <script>
    (function(i,s,o,g,r,a,m){i['FeedbackBlock']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://chat.mclck.com/load.js?u=bWFzdGVybWluZA==&a={!! base64_encode(auth()->guard('admin')->user()->username) !!}','ga');
</script>
@endif
